CREATE DATABASE  IF NOT EXISTS `org_bbc_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `org_bbc_db`;
-- MySQL dump 10.13  Distrib 8.0.16, for macos10.14 (x86_64)
--
-- Host: localhost    Database: bbc_db
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bbc`
--

DROP TABLE IF EXISTS `bbc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `bbc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `region` varchar(60) DEFAULT NULL,
  `area` decimal(10,0) DEFAULT NULL,
  `population` decimal(11,0) DEFAULT NULL,
  `gdp` decimal(14,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bbc`
--

LOCK TABLES `bbc` WRITE;
/*!40000 ALTER TABLE `bbc` DISABLE KEYS */;
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (1,'Afghanistan','South Asia',652225,26000000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (2,'Albania','Europe',28728,3200000,6656000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (3,'Algeria','Middle East',2400000,32900000,75012000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (4,'Andorra','Europe',468,64000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (5,'Angola','Africa',1250000,14500000,14935000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (6,'Antigua and Barbuda','Americas',442,77000,770000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (7,'Argentina','South America',2800000,39300000,146196000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (8,'Armenia','Europe',29743,3000000,3360000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (9,'Australia','Asia-Pacific',7700000,20300000,546070000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (10,'Austria','Europe',83871,8100000,261630000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (11,'Azerbaijan','Europe',86600,8500000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (12,'Bahamas The','Americas',13939,321000,4789320000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (13,'Bahrain','Middle East',717,754000,9357140000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (14,'Bangladesh','South Asia',143998,152600000,67144000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (15,'Barbados','Americas',430,272000,2518720000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (16,'Belarus','Europe',207595,9800000,20776000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (17,'Belgium','Europe',30528,10300000,319609000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (18,'Belize','Americas',22965,266000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (19,'Benin','Africa',112622,7100000,3763000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (20,'Bhutan','South Asia',38364,2400000,1824000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (21,'Bolivia','South America',1100000,9100000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (22,'Bosnia and Herzegovina','Europe',51129,4200000,8568000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (23,'Botswana','Africa',581730,1800000,7812000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (24,'Brazil','South America',8550000,182800000,564852000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (25,'Brunei','Asia-Pacific',5765,374000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (26,'Bulgaria','Europe',110994,7800000,21372000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (27,'Burkina Faso','Africa',274200,13800000,4968000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (28,'Burma','Asia-Pacific',676552,50700000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (29,'Burundi','Africa',27816,7300000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (30,'Cabo Verde','Africa',4033,482000,853140000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (31,'Cambodia','Asia-Pacific',181035,14800000,4736000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (32,'Cameroon','Africa',465458,16600000,13280000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (33,'Canada','North America',9900000,32000000,908480000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (34,'Central African Republic','Africa',622984,3900000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (35,'Chad','Africa',1280000,9100000,2366000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (36,'Chile','South America',756096,16200000,79542000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (37,'China','Asia-Pacific',9600000,1300000000,1677000000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (38,'Colombia','South America',1140000,45600000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (39,'Comoros','Africa',1862,812000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (40,'Congo Democratic Republic of the','Africa',2340000,56000000,6720000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (41,'Congo Republic of the','Africa',342000,3039126,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (42,'Costa Rica','Americas',51100,4300000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (43,'Cote d\'Ivoire','Africa',322462,17100000,13167000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (44,'Croatia','Europe',56594,4400000,28996000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (45,'Cuba','Americas',110860,11300000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (46,'Cyprus','Europe',9250,807000,14187060000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (47,'Czech Republic','Europe',78866,10200000,93330000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (48,'Denmark','Europe',43098,5400000,219510000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (49,'Djibouti','Africa',23200,721000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (50,'Dominica','Americas',751,71000,259150000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (51,'Dominican Republic','Americas',48072,9000000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (52,'Ecuador','South America',272045,13400000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (53,'Egypt','Middle East',1000000,74900000,98119000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (54,'El Salvador','Americas',21041,6700000,15745000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (55,'Equatorial Guinea','Africa',28051,521000,484530000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (56,'Eritrea','Africa',117400,4561599,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (57,'Estonia','Europe',45227,1300000,9113000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (58,'Ethiopia','Africa',1130000,74200000,8162000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (59,'Fiji','Asia-Pacific',18376,854000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (60,'Finland','Europe',338145,5200000,170508000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (61,'France','Europe',543965,60700000,1826463000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (62,'Gabon','Africa',267667,1400000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (63,'Gambia The','Africa',11295,1500000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (64,'Georgia','Europe',69700,5000000,5200000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (65,'Germany','Europe',357027,82500000,2484900000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (66,'Ghana','Africa',238533,21800000,8284000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (67,'Greece','Europe',131957,11000000,182710000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (68,'Grenada','Americas',344,103000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (69,'Guatemala','Americas',108890,13000000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (70,'Guinea','Africa',245857,8800000,4048000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (71,'Guinea-Bissau','Africa',36125,1600000,256000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (72,'Guyana','South America',214969,768000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (73,'Haiti','Americas',27750,8500000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (74,'Holy See (Vatican City)','Europe',0,NULL,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (75,'Honduras','Americas',112492,7200000,7416000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (76,'Hungary','Europe',93030,9800000,81046000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (77,'Iceland','Europe',103000,294000,11354280000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (78,'India','South Asia',3100000,1100000000,682000000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (79,'Indonesia','Asia-Pacific',1900000,225300000,256842000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (80,'Iran','Middle East',1650000,70700000,162610000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (81,'Iraq','Middle East',438317,26500000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (82,'Ireland','Europe',70182,4000000,137120000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (83,'Israel and Palestinian territories','Middle East',20770,3800000,4256000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (84,'Italy','Europe',301338,57200000,1494064000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (85,'Jamaica','Americas',10991,2700000,7830000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (86,'Japan','Asia-Pacific',377864,127900000,4755322000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (87,'Jordan','Middle East',89342,5700000,12198000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (88,'Kazakhstan','Asia-Pacific',2700000,15400000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (89,'Kenya','Africa',582646,32800000,15088000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (90,'Kiribati','Asia-Pacific',810,85000,82450000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (91,'Korea North','Asia-Pacific',122762,22900000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (92,'Korea South','Asia-Pacific',99313,48200000,673836000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (93,'Kuwait','Middle East',17818,2700000,48519000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (94,'Kyrgyzstan','Asia-Pacific',199900,5300000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (95,'Laos','Asia-Pacific',236800,5900000,2301000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (96,'Latvia','Europe',64589,2300000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (97,'Lebanon','Middle East',10452,3800000,18924000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (98,'Lesotho','Africa',30355,1800000,1332000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (99,'Liberia','Africa',99067,3600000,396000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (100,'Libya','Africa',1770000,5800000,25810000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (101,'Liechtenstein','Europe',160,34000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (102,'Lithuania','Europe',65300,3400000,19516000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (103,'Luxembourg','Europe',2586,465000,26146950000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (104,'Macedonia','Europe',25713,2000000,4700000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (105,'Madagascar','Africa',587041,18400000,5520000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (106,'Malawi','Africa',118484,12600000,2142000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (107,'Malaysia','Asia-Pacific',329847,25300000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (108,'Maldives','South Asia',298,338000,848380000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (109,'Mali','Africa',1250000,13800000,4968000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (110,'Malta','Europe',316,397000,4863250000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (111,'Marshall Islands','Asia-Pacific',181,57000,135090000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (112,'Mauritania','Middle East',1040000,3100000,1302000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (113,'Mauritius','Africa',2040,1200000,5568000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (114,'Mexico','North America',1960000,106400000,720328000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (115,'Micronesia Federated States of','Asia-Pacific',700,111000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (116,'Moldova','Europe',33800,4300000,3053000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (117,'Monaco','Europe',2,32000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (118,'Mongolia','Asia-Pacific',1560000,2700000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (119,'Morocco','Middle East',710850,31600000,48032000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (120,'Mozambique','Africa',812379,19500000,4875000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (121,'Namibia','Africa',824292,2000000,4740000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (122,'Nauru','Asia-Pacific',21,9900,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (123,'Nepal','South Asia',147181,26300000,6838000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (124,'Netherlands','Europe',41864,16300000,516710000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (125,'New Zealand','Asia-Pacific',270534,4000000,81240000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (126,'Nicaragua','Americas',120254,5700000,4503000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (127,'Niger','Africa',1270000,12900000,2967000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (128,'Nigeria','Africa',923768,130200000,50778000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (129,'Norway','Europe',323759,4600000,239338000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (130,'Oman','Middle East',309500,3000000,23670000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (131,'Pakistan','South Asia',796095,161100000,96660000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (132,'Palau','Asia-Pacific',508,20000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (133,'Panama','Americas',75517,3200000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (134,'Papua New Guinea','Asia-Pacific',462840,5900000,3422000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (135,'Paraguay','South America',406752,6200000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (136,'Peru','South America',1280000,28000000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (137,'Philippines','Asia-Pacific',300000,82800000,96876000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (138,'Poland','Europe',312685,38500000,234465000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (139,'Portugal','Europe',92345,10500000,150675000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (140,'Qatar','Middle East',11437,628000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (141,'Romania','Europe',238391,22200000,64824000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (142,'Russia','Europe',17000000,141500000,482515000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (143,'Rwanda','Africa',26338,8600000,1892000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (144,'Saint Kitts and Nevis','Americas',269,46000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (145,'Saint Vincent and the Grenadines','Americas',616,152000,655120000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (146,'Samoa','Asia-Pacific',2831,182000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (147,'San Marino','Europe',61,27000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (148,'Sao Tome and Principe','Africa',1001,169000,62530000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (149,'Saudi Arabia','Middle East',2240000,25600000,267008000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (150,'Senegal','Africa',196722,10600000,7102000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (151,'Serbia and Montenegro','Europe',102173,10500000,27510000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (152,'Seychelles','Africa',455,76000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (153,'Sierra Leone','Africa',71740,5300000,1060000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (154,'Singapore','Asia-Pacific',660,4400000,106568000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (155,'Slovakia','Europe',49033,5400000,34992000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (156,'Slovenia','Europe',20273,2000000,29620000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (157,'Solomon Islands','Asia-Pacific',27556,504000,277200000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (158,'Somalia','Africa',637657,10700000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (159,'South Africa','Africa',1220000,45300000,164439000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (160,'Spain','Europe',505988,44100000,935361000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (161,'Sri Lanka','South Asia',65610,19400000,19594000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (162,'Sudan','Middle East',2500000,35000000,18550000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (163,'Suriname','South America',163265,442000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (164,'Swaziland','Africa',17364,1100000,1826000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (165,'Sweden','Europe',449964,8900000,318353000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (166,'Switzerland','Europe',41284,7100000,342433000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (167,'Syria','Middle East',185180,18600000,22134000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (168,'Taiwan','Asia-Pacific',36188,22700000,302364000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (169,'Tajikistan','Asia-Pacific',143100,6300000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (170,'Tanzania','Africa',945087,38400000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (171,'Thailand','Asia-Pacific',513115,64100000,162814000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (172,'Timor-Leste','Asia-Pacific',14609,857000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (173,'Togo','Africa',56785,5100000,1938000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (174,'Tonga','Asia-Pacific',748,106000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (175,'Trinidad and Tobago','Americas',5128,1300000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (176,'Tunisia','Middle East',164150,10000000,26300000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (177,'Turkey','Europe',779452,73300000,274875000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (178,'Turkmenistan','Asia-Pacific',488100,5000000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (179,'Tuvalu','Asia-Pacific',26,10000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (180,'Uganda','Africa',241038,27600000,7452000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (181,'Ukraine','Europe',603700,47800000,60228000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (182,'United Arab Emirates','Middle East',77700,3100000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (183,'United Kingdom','Europe',242514,59600000,2022824000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (184,'United States','North America',9800000,295000000,12213000000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (185,'Uruguay','South America',176215,3500000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (186,'Uzbekistan','Asia-Pacific',447400,26900000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (187,'Vanuatu','Asia-Pacific',12190,222000,297480000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (188,'Venezuela','South America',881050,26600000,NULL);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (189,'Vietnam','Asia-Pacific',329247,83600000,45980000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (190,'Yemen','Middle East',536869,21500000,12255000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (191,'Zambia','Africa',752614,11000000,4950000000);
INSERT INTO `bbc` (`id`, `name`, `region`, `area`, `population`, `gdp`) VALUES (192,'Zimbabwe','Africa',390759,12900000,6192000000);
/*!40000 ALTER TABLE `bbc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'bbc_db'
--

--
-- Dumping routines for database 'bbc_db'
--
/*!50003 DROP PROCEDURE IF EXISTS `clone` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `clone`()
BEGIN
DROP SCHEMA  IF EXISTS bbcdb;
CREATE SCHEMA bbcdb;
CREATE TABLE bbcdb.bbc AS (select * from org_bbc_db.bbc);
ALTER TABLE `bbcdb`.`bbc`  ADD PRIMARY KEY (`id`);
ALTER TABLE `bbcdb`.`bbc` ADD INDEX `idx_bbc_name` (`name` ASC) VISIBLE;
CREATE USER IF NOT EXISTS 'job'@'localhost' IDENTIFIED BY '&job';
GRANT SELECT ON *.* TO  'job'@'localhost';
GRANT ALL PRIVILEGES ON bbcdb.* TO 'job'@'localhost';
GRANT EXECUTE ON PROCEDURE org_bbc_db.clone TO 'job'@'localhost';
-- job ne pourra cependant exécuter cette procédure que si root l'a exécuté avant
-- puisqu'il en reçoit le droit ici
FLUSH PRIVILEGES;
-- select * from bbcdb.bbc;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-21 11:30:01
