-- Réponses aux questions de démarrage

Q1. 

SELECT booking_date as date, nights as 'nuit(s)' FROM booking
where guest_id = 1185;


Q2.

select booking_date as date, arrival_time as arrivée, first_name as prénom, last_name as nom
from booking join guest on (guest.id = booking.guest_id)
where booking_date='2016-11-05'
order by arrival_time;


Q3. 

select booking_date as arrivée, 
           adddate(booking_date, nights) as départ,
          last_name as nom,  first_name as prénom
from booking join guest on (guest.id = booking.guest_id)
where booking_date='2016-11-15'
order by départ, nom;


Q4. 

SELECT room_no as 'chambre', booking_date as date, first_name as 'prénom',last_name as 'nom', address as 'adresse'
FROM booking JOIN guest ON guest_id = guest.id
WHERE room_no=101 AND booking_date='2016-11-17';


Q5.

select id as 'réservation', room_type_requested as 'type', occupants as 'personne(s)', amount as 'prix' from booking 
join rate on(rate.room_type=booking.room_type_requested and rate.occupancy=booking.occupants)
where booking.id in (5152, 5165, 5154, 5295);


Q6. 

select g.last_name, g.first_name, b.id as booking, b.booking_date, b.nights from booking b
join guest g on g.id=b.guest_id
where g.last_name = 'Haselhurst';



Q7.

select g.last_name, g.first_name, count(b.id) as nbr_booking, sum(nights) as total_nights from booking b
join guest g on g.id=b.guest_id
group by g.last_name, g.first_name
having g.first_name like '%Alan%';