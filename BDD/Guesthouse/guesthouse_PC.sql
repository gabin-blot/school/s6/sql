-- Donnez toutes les dates de réservation et le nombre de nuits du client 1185ELECT booking_date AS "date", nights as "nuit(s)" FROM booking WHERE guest_id=1185;

-- Q1. Quand arrivent-ils ? Listez la date d’arrivée, l'heure prévue d'arrivée, le nom et le prénom de tous les clients devant arriver le 2016-11-05. Classez les résultats en fonction de l'heure d'arrivée
SELECT booking_date as date, arrival_time as arrivée, first_name as prénom, last_name as nom
FROM booking JOIN guest ON (guest.id = booking.guest_id)
WHERE booking_date="2016-11-05"
ORDER BY arrival_time;

-- Q2. Quand partent-ils ? Listez la date d’arrivée, le jour de départ, le nom et le prénom de tous les clients devant arriver le 2016-11-15. Classez les résultats en fonction du jour de départ.
SELECT booking_date as date, ADDDATE(booking_date, nights) as départ, last_name as nom, first_name as prénom
FROM booking JOIN guest ON (guest.id = booking.guest_id)
WHERE booking_date="2016-11-15"
ORDER BY départ;

-- Q3. Montrez qui a réservé la chambre 101 le 17/11/2016. Donnez son prénom, son nom et son adresse
SELECT first_name, last_name, address FROM booking 
JOIN guest ON (guest_id=guest.id)
WHERE booking_date="2016-11-17" AND room_no="101";

-- Q4. Vérifier les prix. Montrez le prix par nuit pour les réservations de numéro 5152, 5165, 5154 and 5295. Précisez le numéro de réservation, le type de chambre demandé, le nombre d’occupants et le prix par nuitée
SELECT id, room_type_requested, occupants, amount 
FROM booking JOIN rate ON (room_type_requested=room_type AND occupants=occupancy)
WHERE id IN (5152, 5165, 5154, 5295);

-- Q5. Combien de réservations, combien de nuits ? Montrez toutes les réservations de tous les clients de nom « Haselhurst ». Dans vos résultats faites apparaitre les noms et prénoms du client, le numéro de réservation, le nombre de réservations et le nombre de nuits.
SELECT last_name, first_name, booking.id, booking_date, nights FROM booking JOIN guest ON guest_id=guest.id WHERE last_name="Haselhurst";

-- Q6. Combien de réservations, combien de nuits au total ? Montrez le nombre de réservations et leur nombre total de nuits réservées des clients de prénom contenant « Alan ». Dans vos résultats faites apparaitre les noms et prénoms du client.
SELECT last_name, first_name, count(guest.id) as nbr_booking, sum(nights) as total_nights 
FROM booking JOIN guest ON guest_id=guest.id 
GROUP BY guest_id HAVING first_name LIKE '%Alan%';

-- Questions d'entrainement 


-- Ajouter extras à Q5
SELECT booking.id, room_type_requested, occupants, rate.amount+extra.amount 
FROM booking 
JOIN rate ON (room_type_requested=room_type AND occupants=occupancy) 
JOIN extra ON (booking.id = booking_id)
WHERE booking.id IN (5152, 5165, 5154, 5295);

-- Quels est le chiffre (hors extra) de chaques jours pour le mois de mai 2016, indiquer le nombre de chambres occupées
SELECT SUM(rate.amount+extra.amount), SUM(booking_id),  ADDDATE(booking_date, nights) AS depart
FROM booking 
JOIN rate ON (room_type_requested=room_type AND occupants=occupancy) 
JOIN extra ON (booking.id = booking_id)
GROUP BY depart
HAVING depart LIKE "%2016%";







