SELECT SUM(gdp)/1e9 AS 'PIB Total Europe' FROM bbc WHERE region='Europe';
SELECT ROUND(AVG(gdp/population),0) AS 'Moyenne des PIB/hab Europ' FROM bbc WHERE region='Europe';
SELECT ROUND(SUM(gdp)/SUM(population),0) AS 'PIB/hab Europe' FROM bbc WHERE region='Europe';
SELECT name, ROUND(gdp/population,0) AS 'pib_hab' FROM bbc WHERE region='Europe' AND ROUND(gdp/population,0)<.7*16129;
SELECT count(*) AS 'nbr pays', ROUND(AVG(population/1e6),3) AS 'population moyenne' FROM bbc WHERE region='Americas';
SELECT GROUP_CONCAT(name ORDER BY population DESC SEPARATOR ', ') FROM bbc WHERE region='South America';