-- Exercice 1. Calculez les PIB total de chaque région, en milliards de dollars, classés par valeurs décroissantes.
SELECT region, SUM(gdp)/1e9 as 'PIB Total' FROM bbc GROUP BY region ORDER BY SUM(gdp) DESC;

-- Exercice 2 : Donnez un tableau du nombre de pays par tranche de 5000$ de PIB/Habitant
SELECT 1+(ROUND(gdp/population, 0) DIV 5000) AS tranche, COUNT(name) AS nb_pays FROM bbc 
GROUP BY tranche 
ORDER BY tranche DESC;

-- Exercice 3 : Avec une première requête, calculez la densité de population mondiale. Utilisant la valeur du résultat, montrer les régions de densité supérieure à la moyenne mondiale, classées par densité décroissante.
SELECT region, SUM(population)/SUM(area) as densité FROM bbc 
GROUP BY region
HAVING densité > 48.1191;

-- Exercice 4 : Ecrivez une commande produisant le même résultat que la commande "SELECT DISTINCT region FROM bbc ORDER BY region;", mais sans utilisez le mot clé « distinct ».
SELECT region FROM bbc 
GROUP BY region 
ORDER BY region;
