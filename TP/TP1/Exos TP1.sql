SELECT name,population FROM bbc WHERE name="France";
SELECT name,population, gdp FROM bbc WHERE area < 10000 AND gdp > 500000000;
SELECT name, population, gdp/population FROM bbc WHERE name IN ('Denmark','Finland','Iceland','Norway','Sweden');
SELECT name FROM bbc WHERE name LIKE '%Republic%' OR '%republic%';
SELECT name FROM bbc WHERE name LIKE '%Republic%' AND NOT name LIKE '%Republic' AND NOT name LIKE 'Republic%';
SELECT name, population/area AS density FROM bbc WHERE (population/area BETWEEN 100 AND 120) AND region='Europe';
SELECT region, name, population/1000000 AS 'Pop. (million)' FROM bbc WHERE population > 100000000 ORDER by region ASC, population DESC ;
SELECT concat(name, ' - ', region) AS 'name - region', population/1000000 AS 'Population (millions)' FROM bbc ORDER by population DESC LIMIT 10;