-- Exercice 1. Quels sont les pays d’Europe dont la surface est supérieure ou égale à celle de la France ? Indiquez la surface en milliers de km2.
SELECT name,area/1000 FROM bbc
WHERE region = 'Europe' AND area >= (
SELECT area FROM bbc
WHERE name = 'France');

-- Exercice 2 : Quels sont les pays hors d'Europe plus riches que l'Europe (la richesse étant mesurée en PIB/habitant) ? Classez-les.
SELECT name, ROUND(wealth) AS wealth FROM 
(SELECT name, gdp/population AS wealth FROM bbc WHERE region != 'Europe') tmp 
WHERE wealth >= (SELECT SUM(gdp)/SUM(population) FROM bbc WHERE region = 'Europe' GROUP BY region)
ORDER BY wealth DESC;

-- Exercice 3 : Quelle est la part de chaque pays européens dans le PIB européen total ? Classez les pays par contribution décroissante.
SELECT name AS Pays, gdp/(SELECT SUM(gdp) FROM bbc GROUP BY region HAVING region = 'Europe') AS Pourcent 
FROM bbc WHERE region = 'Europe' 
ORDER BY Pourcent DESC;

-- Exercice 4 : Quel est le plus grand pays de chaque région ? Afficher son nom, sa région et sa surface.
SELECT name, region, area FROM bbc AS bbc1 WHERE area = (SELECT max(area) FROM bbc as bbc2 WHERE bbc1.region = bbc2.region);

-- Exercice 5 : Quels sont les pays du monde plus vastes que la somme des surfaces de tous les autres pays de leur région ?
SELECT region, name, area FROM bbc AS bbc1 WHERE area >= (SELECT SUM(area)-bbc1.area FROM bbc AS bbc2 GROUP BY region HAVING region = bbc1.region) 
ORDER BY region;
